/*!
 * Name    : Canvas Particles
 * Version : 1.0.0
 * Author  : Andrew Ferri <https://andrewferri.com>
 * URL     : https://gitlab.com/wh1tel1te/canvas-particles
 */

function Canvas(el, options){

    let self = this;
    self.canvas = el;
    self.canvas.width = window.innerWidth;
    self.canvas.height = window.innerHeight;
    self.circles = [];

    // Default settings
    self.settings = {
        circles: 1000,
        minRadius: 1,
        maxRadius: 3,
        popupRadius: 10,
        speed: 1,
        glow: true,
        colours: [
            '#F01A30',
            '#F04A58',
            '#FFF7FA',
            '#71DDE3',
            '#0396A6'
        ]
    };

    // Apply options
    if (options !== undefined){
        self.settings = Object.assign(self.settings, options);
    }

    // Dimensions
    self.c = self.canvas.getContext('2d');
    self.width = el.offsetWidth;
    self.height = el.offsetHeight;

    // Create circles
    for (let i = 0; i < self.settings.circles; i++){

        let x = Math.random() * self.width,
            y = Math.random() * self.height,
            dx = (Math.random() - 0.5) * self.settings.speed,
            dy = (Math.random() - 0.5) * self.settings.speed,
            colour = self.settings.colours[Math.floor(Math.random() * self.settings.colours.length)],
            radius = Math.floor(Math.random() * (self.settings.maxRadius - self.settings.minRadius + 1)) + self.settings.minRadius;

        let circle = new CanvasCircle(x, y, dx, dy, colour, radius, self.settings.popupRadius, self.settings.glow, self.c);
        circle.draw();
        self.circles.push(circle);
    }

    self.mouse = {
        x: undefined,
        y: undefined
    };

    // Run
    self.animate();

    // Set height and width on window resize
    window.addEventListener('resize', function(){

        self.canvas.width = window.innerWidth;
        self.canvas.height = window.innerHeight;
        self.width = el.offsetWidth;
        self.height = el.offsetHeight;
    });

    self.canvas.addEventListener('mousemove', function(e){

        self.mouse.x = e.x;
        self.mouse.y = e.y;
    });

    self.canvas.addEventListener('mouseleave', function(e){


    });
}

Canvas.prototype.animate = function(){

    let self = this;
    requestAnimationFrame(self.animate.bind(self));
    self.clear();
    //console.log(self.mouse);
    self.circles.forEach(function(c){
        c.update(self.mouse);
    });
};

Canvas.prototype.clear = function(){

    let self = this;

    // Get canvas pixel data
    let d = self.c.getImageData(0, 0, self.canvas.width, self.canvas.height);

    // Loop through all pixels on the canvas
    for (let i = 0; i < d.data.length; i += 4){

        // Set alpha value to zero
        d.data[i+3] = 0;
    }

    // Put the new data back into the canvas
    self.c.putImageData(d, 0, 0);
};

function CanvasCircle(x, y, dx, dy, colour, radius, max_radius, glow, c){

    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.colour = colour;
    this.radius = radius;
    this.default_radius = radius;
    this.max_radius = max_radius;
    this.glow = glow;
    this.c = c;
    //console.log(this);
}

CanvasCircle.prototype.draw = function(){

    this.c.beginPath();
    this.c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    //this.c.strokeStyle = this.colour;
    //this.c.stroke();
    this.c.fillStyle = this.colour;
    if (this.glow === true){
        this.c.shadowBlur = 10;
        this.c.shadowColor = this.colour;
    }
    this.c.fill();
};

CanvasCircle.prototype.update = function(mouse){

    //console.log(mouse);
    if (this.x + this.radius > window.innerWidth || this.x - this.radius < 0){ this.dx *= -1; }
    if (this.y + this.radius > window.innerHeight || this.y - this.radius < 0){ this.dy *= -1; }

    this.x += this.dx;
    this.y += this.dy;

    if (mouse.x - this.x < 50 && mouse.x - this.x > -50 && mouse.y - this.y < 50 && mouse.y - this.y > -50){
        if (this.radius < this.max_radius){
            this.radius += 1;
        }
    } else if (this.radius > this.default_radius){
        this.radius -= 1;
    }

    this.draw();
};