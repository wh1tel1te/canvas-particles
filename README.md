# Canvas Particles

Creates a particle effect. Based on the tutorials by [Christopher Lis](https://www.chriscourses.com).

## How to run

```
<script src="dist/canvas.min.js"></script>
<script>
new Canvas(document.getElementById('canvas'), settings);
</script>
```

## Settings
| Setting | Values | Default | Description |
| ------- | ------ | ------- | ----------- |
| `circles` | Integer | `1000` | Number of circles to generate |
| `minRadius` | Integer | `1` | Minimum radius size |
| `maxRadius` | Integer | `3` | Maximum radius size |
| `popupRadius` | Integer | `10` | Radius to set circles to when mouse is close by |
| `speed` | Integer | `1` | Speed at which circles move at |
| `glow` | Boolean | `true` | Adds a glow effect to the circles |
| `colours` | Array | `['#F01A30','#F04A58','#FFF7FA','#71DDE3','#0396A6']` | Colours to set the circles to |